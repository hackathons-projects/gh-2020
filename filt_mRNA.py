import argparse
import os
from Bio import SeqIO
from Bio.Blast.Applications import NcbiblastnCommandline
class filt_RNA():
    def __init__(self,file_mRNA,gene,outdir,name):
        self.file_mRNA=file_mRNA
        self.file_gene=gene
        self.outdir=outdir
        self.name=name
        self.outfile=outdir+'/Filt_mRNA.fasta'
        self.outfile2 = outdir + '/mRNA_targ.fasta'
        self.outfile3 = outdir + '/mRNA_targ_gap.fasta'
        self.outfile4 = outdir + '/blast_mRNA.tab'
        self.outfile5 = outdir + '/filt_blast_mRNA.fasta'
        self.mRNA()
        self.filt_ex()
        self.gap()
        self.BLAST()
        self.filt_end()
#сортировка miRNA из MirBase по длине (>=17, но <23), начинаеться только с А/U, выходной файл Filt_mRNA.fasta
    def mRNA(self):
        with open(self.outfile,'w') as outfile:

            for seq in SeqIO.parse(self.file_mRNA,'fasta'):

                len_seq=len(seq.seq)
                if (seq.seq[0].startswith('A') or seq.seq[0].startswith('U')) and len_seq>=17 and len_seq<23:
                    seq0=seq.description.split(' ')
                    seq_id='{0}_{1}_{2}_{3}_{4}'.format(seq0[0],seq0[1],seq0[2],seq0[3],seq0[4])
                    seq_seq=">{0}\n{1}\n".format(seq_id,seq.seq)
                    outfile.write(seq_seq)
#функция берет таблицу с данными по таргетам и микроРНк для рассматриваемого вида, переводит в формат fasta, где в качестве id-
#название транскрипта, а seq- последовательность микроРНК (вместе с гэпами). Выходной файл - mRNA_targ.fasta
    def filt_ex(self):
        with open(self.file_gene) as tab,open(self.outfile2,'w') as output:
            seq_list=[]
            for seq in tab:
                if not seq.startswith('mature'):
                    seq0=seq.split('\t')
                    id_s='>{0}_{1}-{2}'.format(seq0[1],seq0[2],seq0[3])
                    seq_seq=seq0[4]
                    count_gap=seq_seq.count('-')
                    if seq_seq not in seq_list and count_gap<=3:
                        seq_list.append(seq_seq)
                        f_seq='{0}\n{1}\n'.format(id_s,seq_seq)
                        output.write(f_seq)
#Убирает гэпы (обозначены в качестве '-') из файла mRNA_targ.fasta и форммирует последовательности без гэпов. Выходной файл -mRNA_targ_gap.fasta
    def gap(self):
        with open(self.outfile2) as fasta,open(self.outfile3,'w') as output:
            for seq in SeqIO.parse(fasta,'fasta'):
                seq_l=[]
                for s in seq.seq:
                    if not s=='-':
                        seq_l.append(s)
                seq_seq=''.join(seq_l)
                seq1='>{0}\n{1}\n'.format(seq.id,seq_seq)
                output.write(seq1)

#Запуск бласта. Входные файлы - Filt_mRNA.fasta (микроРНК базы данных) и mRNA_targ_gap.fasta (микроРНК рассматриваемого вида).
#Задаем парметры для бласта с word_size=9 (должен быть как минимум в два раза меньше чем выравниваемые последовательности). Запись результатов
#бласта в файл blast_mRNA.tab.
#Далее функция берет таблицу, парсит ее и выделяет только те выравнивания, при которых область выравнивания для двух последовательностей находится в регионе (1,4) до (8,9) или (13,14)
#для таких из файла с микроРНК для данного вида достаем последовательности и записываем в файл filt_blast_mRNA.fasta
    def BLAST(self):
        makeBlastDb = '{0} -in {1} -out {1} -dbtype nucl '.format('makeblastdb', self.outfile)
        run_makeBlastdb = os.system(makeBlastDb)
        blast = NcbiblastnCommandline(query=self.outfile3, db=self.outfile, out=self.outfile4, word_size=9, outfmt='6 qseqid sseqid pident length gapopen qstart qend sstart send qcovhsp', num_threads=150)
        stdout, stderr = blast()

    def filt_end(self):
        with open(self.outfile4) as output, open(self.outfile5, 'w') as output2:
            list_blast = []
            for seq in output:

                seq0 = seq.split('\t')
                if (int(seq0[5]) and int(seq0[7])) in range(1, 5) and (((int(seq0[6]) and int(seq0[8])) in range(8, 10)) or ((int(seq0[6]) and int(seq0[8])) in range(13, 15))) and str(self.name) in seq0[1]:
                    list_blast.append(seq0[0])
            for seq in list_blast:
                print(seq)
            for seq in SeqIO.parse(self.outfile3,'fasta'):
                if seq.id in list_blast:
                    SeqIO.write(seq,output2,'fasta')


if __name__ == "__main__":
    import argparse
    import os
    parser = argparse.ArgumentParser(description='miRNA filtering')
    parser.add_argument("-f",  help="Path to microRNA file")
    parser.add_argument("-G",  help="Path to gene file")
    parser.add_argument("-o",  help="Path to output fasta file")
    parser.add_argument("-n", help="Genus of the investigation organism (for example,Homo)")

    args = parser.parse_args()

    filt_RNA(file_mRNA=args.f,gene=args.G,outdir=args.o, name=args.n)

