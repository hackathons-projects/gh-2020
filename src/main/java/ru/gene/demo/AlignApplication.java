package ru.gene.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AlignApplication {

    public static void main(String[] args) {
        SpringApplication.run(AlignApplication.class, args);
    }
}
