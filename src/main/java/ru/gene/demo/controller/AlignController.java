package ru.gene.demo.controller;

import lombok.RequiredArgsConstructor;
import org.biojava.nbio.core.exceptions.CompoundNotFoundException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.gene.demo.model.AlignRequest;
import ru.gene.demo.service.AlignService;

import java.io.IOException;

@RequiredArgsConstructor
@RestController
public class AlignController {

    private final AlignService alignService;

    @PostMapping("/align")
    public void align(@RequestBody AlignRequest alignRequest) throws CompoundNotFoundException, IOException {
        alignService.align(alignRequest.getRnaFilePath(), alignRequest.getDnaFilePath());
    }
}
