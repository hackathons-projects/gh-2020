package ru.gene.demo.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class AlignRequest {
    private String rnaFilePath;
    private String dnaFilePath;
}
