package ru.gene.demo.model;

public class Hash {

    final int seed;

    public Hash(int size) {
        seed = (int) (Math.floor(Math.random() * size) + 32);
    }

    int hash(String s) {
        int result = 1;
        for (int index = 0; index < s.length(); ++index)
            result = (seed * result + Character.codePointAt(s, index));

        return result;
    }
}
