package ru.gene.demo.model;
import java.util.Collection;
import java.util.function.Function;

public class MinHash<T> {

    private final int functionCount;
    private final Hash[] hashes;

    public MinHash(float maxError) {
        this.functionCount = Math.round(1 / (maxError * maxError));
        this.hashes = new Hash[functionCount];
        for (int index = 0; index < functionCount; index++) {
            hashes[index] = new Hash(index);
        }
    }

    public int findMinHash(Collection<T> set, Function<T, Integer> hashFunction) {
        int min = Integer.MAX_VALUE;
        for (T t : set) {
            int hash = hashFunction.apply(t);
            if (hash < min) min = hash;
        }

        return min;
    }

    public int[] signature(Collection<T> set) {
        int[] result = new int[functionCount];

        for (int i = 0; i < functionCount; i++) {
            final int size = i;
            int minHash = findMinHash(set, t -> hashes[size].hash(t.toString()));
            result[i] = minHash;
        }

        return result;
    }

    public float similarity(Collection<T> a, Collection<T> b) {
        int equalCount = 0;
        int[] signatureA = signature(a);
        int[] signatureB = signature(b);

        for (int i = 0; i < functionCount; i++) {
            if (signatureA[i] == signatureB[i]) {
                equalCount++;
            }
        }

        return (float) equalCount / functionCount;
    }
}
