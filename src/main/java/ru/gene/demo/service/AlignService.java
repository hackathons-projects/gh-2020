package ru.gene.demo.service;

import com.google.common.hash.BloomFilter;
import lombok.RequiredArgsConstructor;
import org.biojava.bio.seq.RNATools;
import org.biojava.bio.symbol.IllegalAlphabetException;
import org.biojava.bio.symbol.IllegalSymbolException;
import org.biojava.bio.symbol.SymbolList;
import org.biojava.nbio.alignment.Alignments;
import org.biojava.nbio.alignment.SimpleGapPenalty;
import org.biojava.nbio.alignment.template.PairwiseSequenceAligner;
import org.biojava.nbio.core.alignment.matrices.SimpleSubstitutionMatrix;
import org.biojava.nbio.core.alignment.template.SequencePair;
import org.biojava.nbio.core.exceptions.CompoundNotFoundException;
import org.biojava.nbio.core.sequence.DNASequence;
import org.biojava.nbio.core.sequence.RNASequence;
import org.biojava.nbio.core.sequence.compound.NucleotideCompound;
import org.biojava.nbio.core.sequence.compound.RNACompoundSet;
import org.biojava.nbio.core.sequence.template.AbstractSequence;
import org.biojava.nbio.data.sequence.FastaSequence;
import org.biojava.nbio.data.sequence.SequenceUtil;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.List;

@RequiredArgsConstructor
@Service
public class AlignService {

    private final int DISCRETIZATION = 4;

    private final HashingService hashingService;

    @SuppressWarnings("UnstableApiUsage")
    public void align(String rnaFilePath, String dnaFilePath) throws IOException, CompoundNotFoundException {
        Files.createDirectories(Path.of("output/"));
        Files.deleteIfExists(Path.of("/output/result.txt"));

        // Read FASTA files
        final List<FastaSequence> rna = SequenceUtil.readFasta(Files.newInputStream(Path.of(rnaFilePath)));
        final List<FastaSequence> dna = SequenceUtil.readFasta(Files.newInputStream(Path.of(dnaFilePath)));
        for (FastaSequence dnaFastaSequence : dna) {
            RNASequence target = new DNASequence(dna.get(0).getSequence()).getRNASequence();
            Files.writeString(Path.of("output/result.txt"), "DNA: " + dnaFastaSequence.getId() + "\n",
                    StandardOpenOption.CREATE, StandardOpenOption.APPEND);

            System.out.println("Length = " + target.getSequenceAsString().length());
            System.out.println("Fpp = " + 1f / target.getSequenceAsString().length());
            BloomFilter<CharSequence> dnaBloomFilter = hashingService.bloomFilter(target.getSequenceAsString(), DISCRETIZATION, 1f / target.getSequenceAsString().length());

            rna.
//                    parallelStream().
        forEach(fastaSequence -> {
    try {
        SymbolList rnaSymbols = RNATools.createRNA(fastaSequence.getSequence());
        SymbolList complement = RNATools.complement(rnaSymbols);
        RNASequence rnaSequence = new RNASequence(complement.seqString());

        short match = 2, gop = -5, gep = -3; // 2, 5, and 3 are coprime; -2 is the mismatch score
        SimpleSubstitutionMatrix<NucleotideCompound> matrix = new SimpleSubstitutionMatrix<NucleotideCompound>(new RNACompoundSet(), match, (short) -match);

        PairwiseSequenceAligner<AbstractSequence<NucleotideCompound>, NucleotideCompound> smithWaterman =
                Alignments.getPairwiseAligner(rnaSequence, target, Alignments.PairwiseSequenceAlignerType.LOCAL, new SimpleGapPenalty(gop, gep), matrix);
        SequencePair<AbstractSequence<NucleotideCompound>, NucleotideCompound> pair = smithWaterman.getPair();

        float similarity = hashingService.similarity(dnaBloomFilter, rnaSequence.getSequenceAsString().toUpperCase(), DISCRETIZATION);

        Files.writeString(Path.of("output/result.txt"), fastaSequence.getId() + " " + fastaSequence.getSequence() +
                        " : LCS = " + pair.getNumSimilars() + ", Distance = " + pair.getPercentageOfIdentity(true) +
                        " BloomFilter similarity: " + similarity + "\n",
                StandardOpenOption.APPEND);
    } catch (IllegalAlphabetException | IllegalSymbolException | CompoundNotFoundException | IOException e) {
        e.printStackTrace();
    }
});
        }
    }
}
