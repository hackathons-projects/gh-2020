package ru.gene.demo.service;

import com.google.common.hash.BloomFilter;
import com.google.common.hash.Funnels;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.stream.IntStream;

@SuppressWarnings("UnstableApiUsage")
@RequiredArgsConstructor
@Service
public class HashingService {

    /**
     * Tokenization
     *
     * @param s - input sequence
     * @param n - token length
     * @return MinHash signature
     */
    public BloomFilter<CharSequence> bloomFilter(String s, int n, float fpp) {
        BloomFilter<CharSequence> bloomFilter = BloomFilter.create(
                Funnels.unencodedCharsFunnel(),
                s.length() - n,
                fpp);

        IntStream.range(0, s.length() - n).parallel().forEach(i -> {
            bloomFilter.put(s.substring(i, i + n));
        });

        System.out.println("Bloom filter ready");
        return bloomFilter;
    }

    public float similarity(BloomFilter<CharSequence> b, String s, int n) {
        int equalCount = 0;

        for (int i = 0; i < s.length() - n; i++) {
            if (b.mightContain(s.substring(i, i + n))) {
                equalCount++;
            }
        }

        return (float) equalCount / (s.length() - n);
    }

    public int coincidences(BloomFilter<CharSequence> b, String s, int n) {
        int equalCount = 0;

        for (int i = 0; i < s.length() - n; i++) {
            if (b.mightContain(s.substring(i, i + n))) {
                equalCount++;
            }
        }

        return equalCount;
    }
}
