package ru.gene.demo;

import org.biojava.bio.seq.RNATools;
import org.biojava.bio.symbol.IllegalAlphabetException;
import org.biojava.bio.symbol.IllegalSymbolException;
import org.biojava.bio.symbol.SymbolList;
import org.biojava.nbio.alignment.Alignments;
import org.biojava.nbio.alignment.SimpleGapPenalty;
import org.biojava.nbio.alignment.template.PairwiseSequenceAligner;
import org.biojava.nbio.core.alignment.matrices.SimpleSubstitutionMatrix;
import org.biojava.nbio.core.alignment.template.SequencePair;
import org.biojava.nbio.core.exceptions.CompoundNotFoundException;
import org.biojava.nbio.core.sequence.DNASequence;
import org.biojava.nbio.core.sequence.RNASequence;
import org.biojava.nbio.core.sequence.compound.NucleotideCompound;
import org.biojava.nbio.core.sequence.compound.RNACompoundSet;
import org.biojava.nbio.core.sequence.template.AbstractSequence;
import org.biojava.nbio.data.sequence.FastaSequence;
import org.biojava.nbio.data.sequence.SequenceUtil;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

@SpringBootTest
class AlignApplicationTests {

    @Test
    void contextLoads() {
    }


    @Test
    public void slm() throws IOException, IllegalSymbolException, IllegalAlphabetException, CompoundNotFoundException {
        // Read FASTA files
        final List<FastaSequence> rna = SequenceUtil.readFasta(Files.newInputStream(Path.of("input/mature.fa")));
//        final List<FastaSequence> dna = SequenceUtil.readFasta(Files.newInputStream(Path.of("input/ncbi_dataset/ncbi_dataset/data/GCF_000001405.39/chr1.fna")));
        final List<FastaSequence> dna = SequenceUtil.readFasta(Files.newInputStream(Path.of("input/ncbi_dataset/ncbi_dataset/data/GCF_000001215.4/chrY.fna")));
        RNASequence target = new DNASequence(dna.get(0).getSequence()).getRNASequence();

        for (FastaSequence fastaSequence : rna) {
            SymbolList rnaSymbols = RNATools.createRNA(fastaSequence.getSequence());
            SymbolList complement = RNATools.complement(rnaSymbols);
            RNASequence rnaSequence = new RNASequence(complement.seqString());

            short match = 2, gop = -5, gep = -3; // 2, 5, and 3 are coprime; -2 is the mismatch score
            SimpleSubstitutionMatrix<NucleotideCompound> matrix = new SimpleSubstitutionMatrix<NucleotideCompound>(new RNACompoundSet(), match, (short) -match);

            PairwiseSequenceAligner<AbstractSequence<NucleotideCompound>, NucleotideCompound> smithWaterman =
                    Alignments.getPairwiseAligner(rnaSequence, target, Alignments.PairwiseSequenceAlignerType.LOCAL, new SimpleGapPenalty(gop, gep), matrix);
            SequencePair<AbstractSequence<NucleotideCompound>, NucleotideCompound> pair = smithWaterman.getPair();

            System.out.println(fastaSequence.getId() + " " + fastaSequence.getSequence() +
                    " : LCS = " + pair.getNumSimilars() + ", Distance = " + pair.getPercentageOfIdentity(true));
        }
    }

    @Test
    public void testComplex() throws Exception {

        short match = 2, gop = -5, gep = -3; // 2, 5, and 3 are coprime; -2 is the mismatch score
        SimpleSubstitutionMatrix<NucleotideCompound> mx = new SimpleSubstitutionMatrix<NucleotideCompound>(new RNACompoundSet(), match, (short) -match);

        RNASequence a = new RNASequence("CGUAU  AUAUCGCGCGCGCGAUAUAUAUAUCU UCUCUAAAAAAA".replaceAll(" ", ""));
        RNASequence b = new RNASequence("GGUAUAUAUAUCGCGCGCACGAU UAUAUAUCUCUCUCUAAAAAAA".replaceAll(" ", ""));

//  mismatches:                             ^              ^
// The two alignments should have the same score. The bottom one is the one the aligner found.

        PairwiseSequenceAligner<RNASequence, NucleotideCompound> aligner =
                Alignments.getPairwiseAligner(a, b, Alignments.PairwiseSequenceAlignerType.GLOBAL, new SimpleGapPenalty(gop, gep), mx);
        SequencePair<RNASequence, NucleotideCompound> pair = aligner.getPair();


        int nMatches = "--CGUAUAUAUCGCGCGCGCGAUAUAUAUAUCU-UCUCUAAAAAAA".length() - 2 - 4;
        double expectedScore = nMatches * match
                - 2 * match // there are two mismatches
                + 3 * gop + 4 * gep; // there are 3 gap opens and either 1 or 4 extensions, depending on the def
        System.out.println(aligner.getScore());
    }

}
