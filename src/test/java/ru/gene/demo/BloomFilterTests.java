package ru.gene.demo;

import com.google.common.hash.BloomFilter;
import com.google.common.hash.Funnels;
import org.apache.commons.math3.util.Pair;
import org.biojava.bio.seq.RNATools;
import org.biojava.bio.symbol.IllegalAlphabetException;
import org.biojava.bio.symbol.IllegalSymbolException;
import org.biojava.bio.symbol.SymbolList;
import org.biojava.nbio.core.exceptions.CompoundNotFoundException;
import org.biojava.nbio.core.sequence.DNASequence;
import org.biojava.nbio.core.sequence.RNASequence;
import org.biojava.nbio.data.sequence.FastaSequence;
import org.biojava.nbio.data.sequence.SequenceUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ru.gene.demo.service.HashingService;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Random;

@SuppressWarnings("UnstableApiUsage")
@SpringBootTest
public class BloomFilterTests {

    private static final int DISCRETIZATION = 6;
    @Autowired
    private HashingService hashingService;

    @Test
    public void complexTest() throws IOException, CompoundNotFoundException, IllegalSymbolException, IllegalAlphabetException {
        final List<FastaSequence> rna = SequenceUtil.readFasta(Files.newInputStream(Path.of("input/mature.fa")));
//        final List<FastaSequence> dna = SequenceUtil.readFasta(Files.newInputStream(Path.of("input/ncbi_dataset/ncbi_dataset/data/GCF_000001405.39/chr1.fna")));
        final List<FastaSequence> dna = SequenceUtil.readFasta(Files.newInputStream(Path.of("input/ncbi_dataset/ncbi_dataset/data/GCF_000001215.4/chrY.fna")));
        RNASequence target = new DNASequence(dna.get(0).getSequence()).getRNASequence();

        System.out.println("Fpp = " + 1f / target.getSequenceAsString().length());

        BloomFilter<CharSequence> dnaBloomFilter3 = hashingService.bloomFilter(target.getSequenceAsString(), 3, 0.05f);
        BloomFilter<CharSequence> dnaBloomFilter7 = hashingService.bloomFilter(target.getSequenceAsString(), 7, 0.05f);
        BloomFilter<CharSequence> dnaBloomFilter10 = hashingService.bloomFilter(target.getSequenceAsString(), 10, 0.05f);
        BloomFilter<CharSequence> dnaBloomFilter15 = hashingService.bloomFilter(target.getSequenceAsString(), 15, 0.05f);


        for (FastaSequence fastaSequence : rna) {
            SymbolList rnaSymbols = RNATools.createRNA(fastaSequence.getSequence());
            SymbolList complement = RNATools.complement(rnaSymbols);
            RNASequence rnaSequence = new RNASequence(complement.seqString());

            System.out.println(fastaSequence.getSequence() + " Bloom probability 3: " + hashingService.similarity(dnaBloomFilter3, fastaSequence.getSequence(), 3));
            System.out.println(fastaSequence.getSequence()+ " Bloom probability 7: " + hashingService.similarity(dnaBloomFilter7, fastaSequence.getSequence(), 7));
            System.out.println(fastaSequence.getSequence() + " Bloom probability 10: " + hashingService.similarity(dnaBloomFilter10, fastaSequence.getSequence(), 10));
            System.out.println(fastaSequence.getSequence() + " Bloom probability 15: " + hashingService.similarity(dnaBloomFilter15, fastaSequence.getSequence(), 15));
            System.out.println();
        }

    }

    @Test
    public void rt(){
        System.out.println(random());
    }

    private String random() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int index = 0; index < 24; index++) {
            stringBuilder.append(randomSymbol());

        }
        return stringBuilder.toString();
    }

    private String randomSymbol() {
        Random r = new Random();
        int low = 0;
        int high = 3;
        int result = r.nextInt(high - low) + low;
        switch (result) {
            case 0:
                return "A";
            case 1:
                return "G";
            case 2:
                return "C";
            default:
                return "U";
        }
    }


    @Test
    public void complexRandomTest() throws IOException, CompoundNotFoundException, IllegalSymbolException, IllegalAlphabetException {
        final List<FastaSequence> dna = SequenceUtil.readFasta(Files.newInputStream(Path.of("input/ncbi_dataset/ncbi_dataset/data/GCF_000001215.4/chrY.fna")));
        RNASequence target = new DNASequence(dna.get(0).getSequence()).getRNASequence();

        System.out.println("Length = " + target.getSequenceAsString().length());
        System.out.println("Fpp = " + 1f / target.getSequenceAsString().length());
        BloomFilter<CharSequence> dnaBloomFilter3 = hashingService.bloomFilter(target.getSequenceAsString(), 3, 0.05f);
        BloomFilter<CharSequence> dnaBloomFilter10 = hashingService.bloomFilter(target.getSequenceAsString(), 10, 0.05f);
        BloomFilter<CharSequence> dnaBloomFilter7 = hashingService.bloomFilter(target.getSequenceAsString(), 7, 0.05f);
        BloomFilter<CharSequence> dnaBloomFilter15 = hashingService.bloomFilter(target.getSequenceAsString(), 15, 0.05f);

        for (int index = 0; index < 100; index++) {
            String random = random();
            System.out.println(random + " Bloom probability 3: " + hashingService.similarity(dnaBloomFilter3, random, 3));
            System.out.println(random + " Bloom probability 7: " + hashingService.similarity(dnaBloomFilter7, random, 7));
            System.out.println(random + " Bloom probability 10: " + hashingService.similarity(dnaBloomFilter10, random, 10));
            System.out.println(random + " Bloom probability 15: " + hashingService.similarity(dnaBloomFilter15, random, 15));
            System.out.println();
        }
    }

    @Test
    public void rnaTest() {
        String s1 = "UGAGGUAGUAGGUUGUAUAGUU";
        String s2 = "CUAUGCAAUUUUCUACCUUACC";
        String s3 = "AAAAAAAAAAAAAAAAAAAAAA";
        String s4 = "UGAGGUAGUAGGUUGCCUUACC";

        int tokenLen = 3;

        BloomFilter<CharSequence> bloomFilter1 = hashingService.bloomFilter(s1, tokenLen, 0.05f);


        System.out.println(s1 + "=" + hashingService.similarity(bloomFilter1, s1, tokenLen));
        System.out.println(s1 + "=" + hashingService.similarity(bloomFilter1, s2, tokenLen));
        System.out.println(s1 + "=" + hashingService.similarity(bloomFilter1, s3, tokenLen));
        System.out.println(s1 + "=" + hashingService.similarity(bloomFilter1, s4, tokenLen));
    }

    @Test
    public void simpleBloomInt() {
        BloomFilter<Integer> bloomFilter =
                BloomFilter.create(Funnels.integerFunnel(), 1000);
        long falsePositiveCount = 0;

        for (int index = 0; index < 1000; index++) {
            if (bloomFilter.mightContain(index)) {
                System.out.println("False positive for: " + index);
                falsePositiveCount++;
            }
            bloomFilter.put(index);
            if (!bloomFilter.mightContain(index)) {
                System.out.println("False negative for: " + index);
            }
        }

        assertEquals("False positive count: ", 0, falsePositiveCount);
    }

    @Test
    @SuppressWarnings("all")
    public void guavaBloomInt() {
        BloomFilter<Integer> bloomFilter = BloomFilter.create(
                Funnels.integerFunnel(),
                1000,
                0.01);
        long falsePositiveCount = 0;

        for (int index = 0; index < 1000; index++) {
            if (bloomFilter.mightContain(index)) {
                System.out.println("False positive for: " + index);
                falsePositiveCount++;
            }
            bloomFilter.put(index);
            if (!bloomFilter.mightContain(index)) {
                System.out.println("False negative for: " + index);
            }
        }

        assertEquals("False positive count: ", 0, falsePositiveCount);
    }

    private void assertEquals(String s, int i, long falsePositiveCount) {
    }

    @Test
    @SuppressWarnings("all")
    public void guavaBloomObject() {
        BloomFilter<byte[]> bloomFilter = BloomFilter.create(
                Funnels.byteArrayFunnel(),
                1000,
                0.01);
        long falsePositiveCount = 0;

        for (Integer index = 0; index < 1000; index++) {
            if (bloomFilter.mightContain(Pair.create(index, index.toString()).toString().getBytes(StandardCharsets.UTF_8))) {
                System.out.println("False positive for: " + index);
                falsePositiveCount++;
            }
            bloomFilter.put(Pair.create(index, index.toString()).toString().getBytes());
            if (!bloomFilter.mightContain(Pair.create(index, index.toString()).toString().getBytes(StandardCharsets.UTF_8))) {
                System.out.println("False negative for: " + index);
            }
        }

        assertEquals("False positive count: ", 0, falsePositiveCount);
    }
}
