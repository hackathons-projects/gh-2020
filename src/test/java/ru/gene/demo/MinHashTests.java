package ru.gene.demo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ru.gene.demo.model.MinHash;
import ru.gene.demo.service.HashingService;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class MinHashTests {

    @Autowired
    private HashingService hashingService;

    @Test
    public void simpleMinHash() {
        MinHash<String> minHash = new MinHash<>(0.05f);

        List<String> text1 = splitText("Lorem ipsum dolor sit amet, at odio noluisse assueverit per, eos invenire conceptam id");
        List<String> text2 = splitText("Lorem ipsum dolor sit amet, at odio noluisse assueverit per");
        List<String> text3 = splitText("Lorem ipsum dolor sit amet");
        List<String> text4 = splitText("Absolutely different text");

        int hash1 = minHash.findMinHash(text1, String::hashCode);
        int hash2 = minHash.findMinHash(text2, String::hashCode);
        int hash3 = minHash.findMinHash(text3, String::hashCode);
        int hash4 = minHash.findMinHash(text4, String::hashCode);

        System.out.println("MinHashes: ");
        System.out.println(hash1);
        System.out.println(hash2);
        System.out.println(hash3);
        System.out.println(hash4);
        System.out.println();

        System.out.println("Signatures:");
        System.out.println(Arrays.toString(minHash.signature(text1)));
        System.out.println(Arrays.toString(minHash.signature(text2)));
        System.out.println(Arrays.toString(minHash.signature(text3)));
        System.out.println(Arrays.toString(minHash.signature(text4)));
        System.out.println();

        System.out.println("Similarity:");
        System.out.println(minHash.similarity(text1, text1));
        System.out.println(minHash.similarity(text1, text2));
        System.out.println(minHash.similarity(text1, text3));
        System.out.println(minHash.similarity(text1, text4));
        System.out.println();

        assertEquals(1f, minHash.similarity(text1, text1), 0.0);
        assertEquals(0f, minHash.similarity(text1, text4), 0.0);
    }

    private List<String> splitText(String s) {
        return Arrays.stream(s.split(" ")).map(String::intern).collect(Collectors.toList());
    }

    @Test
    public void pojoTest() {
        MinHash<Pojo> minHash = new MinHash<>(0.05f);

        List<Pojo> text1 = splitTextToPojo("Lorem ipsum dolor sit amet, at odio noluisse assueverit per, eos invenire conceptam id");
        List<Pojo> text2 = splitTextToPojo("Lorem ipsum dolor sit amet, at odio noluisse assueverit per");
        List<Pojo> text3 = splitTextToPojo("Lorem ipsum dolor sit amet");
        List<Pojo> text4 = splitTextToPojo("Absolutely different text");

        int hash1 = minHash.findMinHash(text1, Pojo::hashCode);
        int hash2 = minHash.findMinHash(text2, Pojo::hashCode);
        int hash3 = minHash.findMinHash(text3, Pojo::hashCode);
        int hash4 = minHash.findMinHash(text4, Pojo::hashCode);

        System.out.println("MinHashes: ");
        System.out.println(hash1);
        System.out.println(hash2);
        System.out.println(hash3);
        System.out.println(hash4);
        System.out.println();

        System.out.println("Signatures:");
        System.out.println(Arrays.toString(minHash.signature(text1)));
        System.out.println(Arrays.toString(minHash.signature(text2)));
        System.out.println(Arrays.toString(minHash.signature(text3)));
        System.out.println(Arrays.toString(minHash.signature(text4)));
        System.out.println();

        System.out.println("Similarity:");
        System.out.println(minHash.similarity(text1, text1));
        System.out.println(minHash.similarity(text1, text2));
        System.out.println(minHash.similarity(text1, text3));
        System.out.println(minHash.similarity(text1, text4));
        System.out.println();

        assertEquals(1f, minHash.similarity(text1, text1), 0.0);
        assertEquals(0f, minHash.similarity(text1, text4), 0.0);
    }

    private List<Pojo> splitTextToPojo(String s) {
        return Arrays.stream(s.split(" ")).map(Pojo::new).collect(Collectors.toList());
    }

}
