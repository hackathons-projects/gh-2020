package ru.gene.demo;

import java.util.Objects;
import java.util.UUID;

public class Pojo {
    private String value;
    private UUID uuid = UUID.randomUUID();

    public Pojo(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pojo pojo = (Pojo) o;
        return Objects.equals(value, pojo.value) &&
                Objects.equals(uuid, pojo.uuid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value, uuid);
    }

    @Override
    public String toString() {
        return "Pojo{" +
                "value=" + value +
                ", uuid=" + uuid +
                '}';
    }
}
